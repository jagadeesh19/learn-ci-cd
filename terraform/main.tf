﻿terraform {
  backend "s3" {
    bucket = "terraform-state-jagadeesh"
    key = "terraform-demo.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "us-west-2"
}

data "aws_ami" "amazon-linux-2-ecs" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-2.*-x86_64*"]
  }
  owners = ["amazon"]
}

data "terraform_remote_state" "state" {
  backend = "s3"

  config = {
    acl    = "bucket-owner-full-control"
    bucket = "terraform-state-jagadeesh"
    key = "terraform-demo.tfstate"
    region = "us-west-2"
  }

}

resource "aws_instance" "ci-cd-instance" {
  ami = data.aws_ami.amazon-linux-2-ecs.id
  instance_type = "t2.micro"
  key_name = "Hello"
  iam_instance_profile = "ecsInstanceRole"
  user_data = <<EOT
  #!/bin/bash
  sudo su
  echo ECS_CLUSTER= cluster-ci-cd >> /etc/ecs/ecs.config
  EOT
}

resource "aws_ecs_cluster" "cluster-ci-cd" {
  name = "cluster-ci-cd"
}

resource "aws_ecs_task_definition" "demo-task" {
  container_definitions = jsonencode([{
    name = "demo-ci-cd"
    image = var.container_image
    essential = true
    memory = 300
    portMappings = [
      {
        containerPort = 80
        hostPort = 80
      }
    ]
  }])
  family = "demo-task"
  requires_compatibilities = ["EC2"]
  cpu = "250"
  memory = "300"
}


resource "aws_ecs_service" "demo-service" {
  name = "demo-service"
  launch_type = "EC2"
  desired_count = 1
  cluster = aws_ecs_cluster.cluster-ci-cd.arn
  task_definition = aws_ecs_task_definition.demo-task.arn
  deployment_minimum_healthy_percent = 0
  deployment_maximum_percent = 200
  ordered_placement_strategy {
    type = "spread"
    field = "host"
  }
}


