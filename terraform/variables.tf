﻿variable "container_image" {
  default = ""
  type = string
}

variable "remote_state_address" {
  default = ""
  type = string
}
variable "username" {
  default = ""
  type = string
}
variable "access_token" {
  default = ""
  type = string
}